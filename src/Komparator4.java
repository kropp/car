import java.util.Comparator;

   class Komparator4 implements Comparator<Car>{
	   
    public int compare(Car c1, Car c2) {
      return  c1.Brand.compareTo(c2.Brand);
    }
    
   }
