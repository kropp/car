import java.util.Comparator;

public class Komparator implements Comparator<Car> {
	public int compare(Car samochod, Car samochod2) {
		if(samochod2 == null) return -1;
		if(samochod.getDate() > samochod2.getDate()) return 1;
		else if(samochod.getDate() < samochod2.getDate()) return -1;
		else return 0;
	}
}