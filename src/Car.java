
public class Car {
	String Brand;
	String model;
	private int date;
	private int price;
	
	public Car(String Brand, String model, int date, int price){
		this.Brand = Brand;
		this.model = model;
		this.date = date;
		this.price = price;
	}

	public String getBrand() {
		return Brand;
	}

	public String getModel() {
		return model;
	}

	public int getDate() {
		return date;
	}

	public int getPrice() {
		return price;
	}

	public String toString() {
		return "Brand: "+Brand+", Model: "+model+", Date: "+date+", Price: "+price;
	}

	public static void show(Car[] cars){	
	for(Car list: cars)
		System.out.println(list);	
	}
}
