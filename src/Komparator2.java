import java.util.Comparator;

public class Komparator2 implements Comparator<Car> {
	public int compare(Car samochod, Car samochod2) {
		if(samochod2 == null) return -1;
		if(samochod.getPrice() > samochod2.getPrice()) return 1;
		else if(samochod.getPrice() < samochod2.getPrice()) return -1;
		else return 0;
	}
}