import java.util.Comparator;

   class Komparator3 implements Comparator<Car>{
	   
    public int compare(Car c1, Car c2) {
      return  c1.model.compareTo(c2.model);
    }
    
   }
