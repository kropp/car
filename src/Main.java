import java.util.Arrays;

public class Main {
	public static void main(String args[]){
	Car car = new Car("VW", "Golf V", 2005 , 88000 );
	Car car2 = new Car("Peugeot", "Boxer", 2011 , 77777);
	Car car3 = new Car("Fiat", "Panda", 2012 , 23534 );
	Car car4 = new Car("Chevrolet", "Camaro", 2013 , 365000);
	Car car5 = new Car("Polonez", "Caro", 1988 , 3500 );
	Car car6 = new Car("VW", "Passat", 2002 , 13500);
	
	Car[] cars = {car, car2, car3, car4, car5, car6};
	
	Komparator byPrice = new Komparator();
	Komparator2 byDate = new Komparator2();
	Komparator3 byModel = new Komparator3();	
	Komparator4 byBrand = new Komparator4();
	
	Arrays.sort(cars, byPrice); 
	Car.show(cars);
	System.out.println("");
	Arrays.sort(cars, byDate); 
	Car.show(cars);
	System.out.println("");
	Arrays.sort(cars, byModel);
	Car.show(cars);
	System.out.println("");
	Arrays.sort(cars, byBrand);
	Car.show(cars);
	
	}
	
}